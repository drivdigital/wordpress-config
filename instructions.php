<!DOCTYPE html>
<html>
<head>
  <title>Installation instructions</title>
</head>
<body>
  <main>
    <h1>Installation instructions</h1>
    <div>
      <p>You need to copy the <em>.env.example</em> file and rename it <em>.env</em>.</p>
    </div>
    <div>
      <p>If you're updating the wordpress config you have to copy over the new drivdigital/wordpress-template files</p>
    </div>
  </main>
</body>
</html>

